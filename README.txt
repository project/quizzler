/* $Id:$ */

The Quizzler module allows you to associate quiz questions with node instances.

You may control who may create questions, and who may take quizzes.  

Output may be easily modified through the standard themeing procedure.

Quiz statistics are automatically generated per user and as a conglomerate.

See the quizzler.module file for more information.

Author
------
Aaron Craig
aaron@evolving-design.com

Sponsor
------
Evolving Design (evolving-design.com)
Learning TV (learning-tv.com)